#!/usr/bin/env iocsh.bash
require(modbus)
require(s7plc)
require(calc)
epicsEnvSet(labs-embla_chop-chic-02_VERSION,"plcfactory")
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")
iocshLoad("./labs-embla_chop-chic-02.iocsh","IPADDR=172.30.235.11,RECVTIMEOUT=3000,SAVEFILE_DIR=/var/log/labs-embla_chop-chic-02")
iocInit()
#EOF